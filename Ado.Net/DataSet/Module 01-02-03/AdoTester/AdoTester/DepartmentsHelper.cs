﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Ado.Net
using System.Data;
using System.Data.Common;
//Драйвер
using System.Data.SQLite;

namespace AdoTester
{
    public class DepartmentsHelper
    {
        //DataView - представление данных - для интерфейсных компонентов
        //WinForms: DataGridView, ComboBox; WPF: GridView
        public static DataView GetDepartments()
        {
            //DataSet - отсоединенный режим работы в Ado.Net
            DataSet data;//"аналог" базы данных
            data = new DataSet("DB");
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            #region Создание DataSet вручную
            ///*
            //
            // Структуры таблиц в базе данных
            //
            DataTable table = new DataTable("Departments");
            table.Columns.Add("DepartmentsID", typeof(int));//
            table.Columns.Add("DepartmentsName", typeof(string));//
            table.Columns.Add("DepartmentsLocation", typeof(string));//
            //первичные ключи: и другие ограничения целостности
            DataColumn[] keys = new DataColumn[1];//если ключ составной
                                                  //на два и более столбцов,
                                                  //то размер массива другой - DataColumn[3];
            keys[0] = table.Columns["DepartmentsID"];
            //keys[1] = table.Columns["RegionsID"];
            table.PrimaryKey = keys;//первичные ключи - это массив
            table.Columns["DepartmentsName"].AllowDBNull = false;
            table.Columns["DepartmentsName"].Unique = true;
            table.Columns["DepartmentsName"].Caption = "Название Отдела";
            //
            //добавление данных (совпадаем с реальной базой данных)
            //
            table.Rows.Add(1, "Head Office", "New York");
            table.Rows.Add(2, "IT Dep", "Kharkiv");
            table.Rows.Add(3, "Accoun Dep", "London");
            data.Tables.Add(table);
            //*/
            #endregion
            #region Заполнение Data из базы данных
            ///*
            var connectionString = "Data Source=d:\\MyData\\MyData;";
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter("select * from Departments", 
                                   connection);
            masterDataAdapter.Fill(data, "Departments2");
            //*/
            #endregion
            data.WriteXml("dataset.dat");
            //data.ReadXml("dataset.dat");
            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }

    }
}
