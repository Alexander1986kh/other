﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Data.Entity;

namespace CF.ManyToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SuperMarketContext db = new SuperMarketContext())
            {
                Product P1 = new Product();
                P1.ProductName = "Dell products";

                Vendor V1 = new Vendor();
                V1.VendorName = "Vivek";
                V1.VendorProducts.Add(P1);
                db.Vendor.Add(V1);
                db.SaveChanges();
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
        }

    }

    #region Entities
    public class Vendor
    {
        public Vendor()
        {
            VendorProducts = new List<Product>();
        }

        public virtual int VendorId
        {
            get;
            set;
        }
        public virtual string VendorName
        {
            get;
            set;
        }
        public virtual ICollection<Product> VendorProducts
        {
            get;
            set;
        }
    }
    public class Product
    {
        public Product()
        {
            ProductVendors = new List<Vendor>();
        }
        public virtual int ProductId
        {
            get;
            set;
        }
        public virtual string ProductName
        {
            get;
            set;
        }
        public virtual ICollection<Vendor> ProductVendors
        {
            get;
            set;
        }
    }
    #endregion
    public class SuperMarketContext : DbContext
    {
       #region

        public SuperMarketContext()
    : base("name=ConnString")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Vendor>()
        .HasMany(v => v.VendorProducts)
        .WithMany(p => p.ProductVendors)
        .Map(
        m =>
        {
            m.MapLeftKey("VendorId");
            m.MapRightKey("ProductId");
            m.ToTable("VendorProduct");
        });

        }

        #endregion
    }
}
