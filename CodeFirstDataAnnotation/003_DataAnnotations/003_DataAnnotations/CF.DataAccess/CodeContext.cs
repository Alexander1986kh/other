﻿using System.Data.Entity;
using CF.Data;

namespace CF.DataAccess
{
    public class CodeContext : DbContext
    {
        static CodeContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CodeContext>());
            //Пересоздать, если поменялись данные
            //DropCreateDatabaseAlways<CodeContext>
            //Database.SetInitializer(new DropCreateDatabaseAlways<CodeContext>());
            //Для автоматизации тестирования
        }

        public CodeContext(): base("dbContext")
        {           
        }

        public DbSet<Attendee> Attendees { get; set; }

    }
}
