﻿using System;

namespace CF.Data
{
    //Обычный класс .Net Классом сущностью он становится
    // при помощи fluent api в метод OnModelCreating
    //класса-наследника DbContext
    //Fluent Api- аналог атрибутов Data Annotation
    public class Attendee
    {
        public int AttendeeTrackingID { get; set; }
        public string LastName { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
