﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EFConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDataModel _entities = new MyDataModel();
            _entities.Employees.Load();
            List<Employees> emp = new List<Employees>();
            foreach(Employees e in _entities.Employees)
            {
                emp.Add(e);
            }
            foreach (Employees e in emp)
            {
                Console.WriteLine(e);
            }
        }
    }
}
