﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesDepartmentsCompanySelectTest
{
    class Program
    {
        static void Main(string[] args)
        {
            #region создание Компании
            var company = new Company
            {
                Name = "A",
                Departments =
                {
                    new Department
                    {
                        Name = "Development",
                        Employees =
                        {
                            new Employee { Name = "T", Salary = 75000m },
                            new Employee { Name = "D", Salary = 45000m },
                            new Employee { Name = "M", Salary = 150000m }// m - Деньги
                        }
                    },
                    new Department
                    {
                        Name = "Marketing",
                        Employees =
                        {
                            new Employee { Name = "Z", Salary = 200000m },
                            new Employee { Name = "X", Salary = 120000m }
                        }
                    }
                }
            };
            #endregion
            #region Пример Linq to Objects
            var query = company.Departments
                               .Select(dept => new { dept.Name,
                                                     Cost = dept.Employees.Sum(person => 
                                                                               person.Salary) })
                               .OrderByDescending(deptWithCost => deptWithCost.Cost);
            /*
             по частям
             company.Departments - все элементы коллекции (FROM часть Selecta из SQL)
             секции WHERE здесь нет
             секция Select в LinqTo используется для создания НОВОГО объекта - 
                                    НОВОГО представления - 99% при помощи анонимных классов

             Select(dept - это элемент из коллекции company.Departments - объект класса Department
                    =>  - после стрелки основании данных из dept создается новый класс или переменная
                    new { dept.Name,
                                                     Cost = dept.Employees.Sum(person => 
                                                                               person.Salary) })
               анонимный класс сделает тоже самое, что и (и является заменой этого кода):
               class Dep2 
               {
                   public string Name {set;get;}
                   public decimal Cost {set;get;}
               }



             Секция Сортировки OrderByDescending
             
            */
            //Вывод на экран Названий Подразделений и Расходов на ЗП сотрудников в Отделе
            foreach (var item in query)
            {
                Console.WriteLine(item);//неявно вызвается метод ToString из анонимного класса
            }
            Console.Write("Press any key...");
            Console.ReadKey(true);
            #endregion

        }
    }
    #region Классы
    class Employee
    {
        public string Name { get; set; }
        public decimal Salary { get; set; }//Деньги
    }

    class Department
    {
        public string Name { get; set; }

        List<Employee> employees = new List<Employee>();

        public IList<Employee> Employees
        {
            get { return employees; }
        }
    }

    class Company
    {
        public string Name { get; set; }

        List<Department> departments = new List<Department>();

        public IList<Department> Departments
        {
            get { return departments; }
        }
    }
    #endregion
}
