﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;//System.Data.SQLite.dll from c:\Program Files\System.Data.SQLite\2010\bin\
using System.IO;

using System.Configuration;//System.Configuration
using System.Data.Odbc;

namespace MasterDetailExample
{
    public partial class Form1 : Form
    {
        private DataGridView masterDataGridView = new DataGridView();
        private BindingSource masterBindingSource = new BindingSource();
        private DataGridView detailsDataGridView = new DataGridView();
        private BindingSource detailsBindingSource = new BindingSource();
        //
        private List<Department> Departments = new List<Department>();
        private List<Employee> Employees = new List<Employee>();
        private Dictionary<Department, IEnumerable<Employee>> GroupEmployee;
        //
        public Form1()
        {
            InitializeComponent();
            masterDataGridView.Dock = DockStyle.Fill;
            detailsDataGridView.Dock = DockStyle.Fill;

            SplitContainer splitContainer1 = new SplitContainer();
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Orientation = Orientation.Horizontal;
            splitContainer1.Panel1.Controls.Add(masterDataGridView);
            splitContainer1.Panel2.Controls.Add(detailsDataGridView);

            this.Controls.Add(splitContainer1);
            this.Load += new System.EventHandler(Form1_Load);
            this.Text = "DataGridView master/detail demo";
            //
            //this.masterBindingSource.CurrentChanged += new System.EventHandler(this.bsDepartments_CurrentChanged);//для списка
            //GetDataFromLists();
            //
            //
        }
        private void Form1_Load(object sender, System.EventArgs e)
        {
            // Bind the DataGridView controls to the BindingSource
            // components and load the data from the database.
            masterDataGridView.DataSource = masterBindingSource;
            detailsDataGridView.DataSource = detailsBindingSource;
            GetDataFromDataBase();
            //
            //GetDataFromLists();
            //
            // Resize the master DataGridView columns to fit the newly loaded data.
            masterDataGridView.AutoResizeColumns();

            // Configure the details DataGridView so that its columns automatically
            // adjust their widths when the data changes.
            detailsDataGridView.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void GetDataFromDataBase()
        {
            try
            {

                var connectionString = "Data Source=d:\\MyData\\MyData;";
                SQLiteConnection connection =
                    new SQLiteConnection(connectionString);
                connection.Open();

                DataSet data = new DataSet();
                data.Locale = System.Globalization.CultureInfo.InvariantCulture;

                SQLiteDataAdapter masterDataAdapter = new
                    SQLiteDataAdapter("select * from Departments", connection);
                masterDataAdapter.Fill(data, "Departments");

                SQLiteDataAdapter detailsDataAdapter = new
                    SQLiteDataAdapter("select * from Employees", connection);
                detailsDataAdapter.Fill(data, "Employees");
                #region Работа с Relations
                DataRelation relation = new DataRelation("EmployeesDepartments",
                    data.Tables["Departments"].Columns["DepartmentsID"],
                    data.Tables["Employees"].Columns["DepartmantsID"]);
                data.Relations.Add(relation);

                #endregion
                DataView dv = data.Tables["Departments"].DefaultView;
                masterBindingSource.DataSource = dv;
                detailsBindingSource.DataSource = masterBindingSource;
                detailsBindingSource.DataMember = "EmployeesDepartments";
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system. : " + ex.ToString());
            }
        }
        #region заготовка для LinqTo и DataRow.Filter
        private void GetDataFromLists()
        {
            
        }
        private void loadDepartments()
        {
            Departments.Add(new Department(10, "Sales", "Hyderabad"));
            Departments.Add(new Department(20, "Purchases", "Mumbai"));
            Departments.Add(new Department(30, "Admin", "Mumbai"));
            Departments.Add(new Department(40, "Accounts", "Mumbai"));
            Departments.Add(new Department(50, "Training", "Hyderabad"));
            Departments.Add(new Department(60, "Stores", "Hyderabad"));
        }
        private void loadEmployees()
        {
            Employees.Add(new Employee(1, "Krishna Prasad", 10, "Manager", 30000));
            Employees.Add(new Employee(2, "Rajesh", 10, "Clerk", 10000));
            Employees.Add(new Employee(3, "Ramesh", 10, "Assistant", 5000));
            Employees.Add(new Employee(4, "Ragesh", 10, "Computer Operator", 4000));

            Employees.Add(new Employee(5, "Murali", 20, "Manager", 30000));
            Employees.Add(new Employee(6, "Anil", 20, "Clerk", 10000));

            Employees.Add(new Employee(7, "Karthik", 30, "Manager", 30000));
            Employees.Add(new Employee(8, "Anirudh", 30, "Assistant", 7000));

            Employees.Add(new Employee(9, "Sarma Chada", 40, "Manager", 15000));
            Employees.Add(new Employee(10, "Anupama", 40, "Assistant", 3000));
            Employees.Add(new Employee(10, "Anirudh", 40, "Accountant", 7000));

            Employees.Add(new Employee(11, "Sailesh", 60, "Store Keeper", 4000));
        }
        #endregion
        private void bsDepartments_CurrentChanged(object sender, EventArgs e)
        {
            if (this.GroupEmployee != null)
               detailsBindingSource.DataSource = this.GroupEmployee[(Department)masterBindingSource.Current];
        }

        
    }
    #region Классы
    public class Department
    {
        private int deptNo = 0;
        private String deptName = String.Empty;
        private String deptLoc = String.Empty;

        public Department() { }

        public Department(int DeptNo, String DeptName, String DeptLoc)
        {
            deptNo = DeptNo;
            deptName = DeptName;
            deptLoc = DeptLoc;
        }

        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String DeptName
        {
            set { deptName = value; }
            get { return deptName; }
        }

        public String DeptLoc
        {
            set { deptLoc = value; }
            get { return deptLoc; }
        }


    }
    public class Employee
    {
        private int employeeNo;
        private String employeeName;
        private int deptNo;
        private String job;
        private Double salary;

        public Employee() { }

        public Employee(int EmployeeNo, String EmployeeName, int DeptNo, String Job, Double Salary)
        {
            employeeNo = EmployeeNo;
            employeeName = EmployeeName;
            deptNo = DeptNo;
            job = Job;
            salary = Salary;
        }
        public int EmployeeNo
        {
            set { employeeNo = value; }
            get { return employeeNo; }
        }
        public String EmployeeName
        {
            set { employeeName = value; }
            get { return employeeName; }
        }
        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String Job
        {
            set { job = value; }
            get { return job; }
        }

        public Double Salary
        {
            set { salary = value; }
            get { return salary; }
        }
    }
    #endregion
}
