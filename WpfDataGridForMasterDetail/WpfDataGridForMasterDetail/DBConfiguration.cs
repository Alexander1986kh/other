﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DatabaseAccess
{
    public class DBConfiguration
    {
        private static string dbConnectionString;
        private static string dbProviderName;

        static DBConfiguration()
        {
            dbConnectionString = ConfigurationManager.ConnectionStrings["MyData"].ConnectionString;            
        }

        public static string DbConnectionString
        {
            get { return dbConnectionString; }
        }

        public static string DbProviderName
        {
            get { return dbProviderName; }
        }

    }
}
