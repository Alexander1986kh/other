﻿using DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfDataGridForMasterDetail
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataSet data;
        private DataView detailDataView;
        public MainWindow()
        {
            InitializeComponent();
            MakeDataSet();
            DataView dv = data.Tables["Departments"].DefaultView;
            dgDepartments1.ItemsSource = dv;
            /*selectFirstRowInDataGrid();
            selectRowInDataGrid();

            //dgEmployees1.ItemsSource = data.Tables["Employees"].DefaultView;
            test_comboBoxColum.ItemsSource = dv;
            test_comboBoxColum.DisplayMemberPath = "DepartmentsName";

            dgDepartments2.ItemsSource = dv;
            // Bind the details data connector to the master data connector,*/
        }
        private void MakeDataSet()
        {
            var connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();

            // Create a DataSet.
            data = new DataSet();
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;

            // Add data from the Customers table to the DataSet.
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter("select * from Departments", connection);
            masterDataAdapter.Fill(data, "Departments");

            // Add data from the Orders table to the DataSet.
            SQLiteDataAdapter detailsDataAdapter = new
                SQLiteDataAdapter("select * from Employees", connection);
            detailsDataAdapter.Fill(data, "Employees");

            // Establish a relationship between the two tables.
            DataRelation relation = new DataRelation("EmployeesDepartments",
                data.Tables["Departments"].Columns["DepartmentsID"],
                data.Tables["Employees"].Columns["DepartmantsID"]);
            data.Relations.Add(relation);
        }
        private void dgDepartments1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dgEmployees1.ItemsSource = curRow.CreateChildView(data.Relations["EmployeesDepartments"]);
            }
            catch (Exception)
            { }
        }
        //
        private SolidColorBrush hb = new SolidColorBrush(Colors.Orange);
        private SolidColorBrush nb = new SolidColorBrush(Colors.White);
        private void dgDepartments1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // Product product = (Product)e.Row.DataContext;
            try
            {
                DataRowView curRow = (DataRowView)(e.Row.DataContext);
                if (curRow["DepartmentsLocation"].ToString().Contains("Kharkiv"))
                    e.Row.Background = hb;
                else
                    e.Row.Background = nb;
            }
            catch (Exception)
            {

            }
        }
        private void dgEmployees1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            //if (row == null) return;

            int rowIndex = dgEmployees1.SelectedIndex;
            DataRowView curRow = (DataRowView)dgEmployees1.SelectedItem;
            DataRow dr = curRow.Row;
            EditEmployeeWindow form = new EditEmployeeWindow(dr, data);
            form.ShowDialog();
        }
    }
    //
    public class DepartmentsHelper
    {
        DataSet data;
        public DataView GetDepartments()
        {
            var connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();

            // Create a DataSet.
            data = new DataSet();
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;

            // Add data from the Customers table to the DataSet.
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter("select * from Departments", connection);
            masterDataAdapter.Fill(data, "Departments");

            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }
    }
}
