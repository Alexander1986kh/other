﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for EditEmployee.xaml
    /// </summary>
    public partial class AddOrEditEmployee : Window
    {
        private bool addFlag = false; 
        public AddOrEditEmployee()
        {
            //adding mode
            InitializeComponent();
            btnStart.Content = "Add";
            addFlag = true;
            txtID.IsEnabled = false;
        }

        public AddOrEditEmployee(int ID, string name, string phone, string mail, int salary, int deptID)
        {
            //edit mode
            InitializeComponent();
            txtID.Text = ID.ToString();
            txtName.Text = Name;
            txtPhone.Text = phone;
            txtMail.Text = mail;
            txtSalary.Text = salary.ToString();
            txtDeptID.Text = deptID.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int ID = -1;
            Int32.TryParse(txtID.Text, out ID);
            int salary = -1;
            Int32.TryParse(txtSalary.Text, out salary);
            int deptID = -1;
            Int32.TryParse(txtDeptID.Text, out deptID);

            if (ID <= -1 || salary <= -1 || deptID <= -1)
            {
                string msg = ID <= -1 ? "Wrong ID!" :
                    salary <= -1 ? "Wrong salary!" :
                   "Wrong department ID!";
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string name = txtName.Text;
            string phone = txtPhone.Text;
            string mail = txtMail.Text;

            if (addFlag)
            {
                //adding mode
                DataRow newRow = DBAccess.data.Tables["EMPLOYEES"].NewRow();
                newRow["EmployeesName"] = "Ivanov 4";
                newRow["DepartmantsID"] = "2";
                newRow["Job"] = "-";
                DBAccess.data.Tables["Employees"].Rows.Add(newRow);
                DBAccess.insertEmployee();

            }
            else
            {
                //edit mode
                DataRow[] row = DBAccess.data.Tables["EMPLOYEES"].Select(string.Format("EMPLOYEES_ID = {0}", ID));
                //editing dataset:
                row[0][1] = name;
                row[0][2] = phone;
                row[0][3] = mail;
                row[0][4] = salary;
                row[0][5] = deptID;

                DBAccess.data.AcceptChanges();
                DBAccess.updateEmployees();
            }

            this.Close();
        }
    }
}
