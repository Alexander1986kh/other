﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class DBAccess
    {
        public static DataSet data;

        static DBAccess()
        {
            data = new DataSet("DB");
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            fillDataSet();
        }

        public static void fillDataSet()
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["MSSQL_home"].ConnectionString;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    SqlDataAdapter adapter;

                    adapter = new SqlDataAdapter(BanksHelper.selectAll(), con);
                    adapter.Fill(data, "BANKS");

                    adapter = new SqlDataAdapter(AccountsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS");

                    // Establish a relationship between the two tables.
                    DataRelation relationBanksAccounts = new DataRelation("BanksAccounts",
                        data.Tables["BANKS"].Columns["BANKS_ID"],
                        data.Tables["ACCOUNTS"].Columns["BANKS_ID"]);
                    data.Relations.Add(relationBanksAccounts);

                    adapter = new SqlDataAdapter(ClientsHelper.selectAll(), con);
                    adapter.Fill(data, "CLIENTS");

                    adapter = new SqlDataAdapter(AccountsToClientsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS_TO_CLIENTS");
                    
                    // Establish many-to-many relationship between tables.
                    DataRelation relationAccountsToClients = new DataRelation("Accounts_AccountsToClients",
                        data.Tables["ACCOUNTS"].Columns["ACCOUNTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["ACCOUNTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);

                    relationAccountsToClients = new DataRelation("Clients_AccountsToClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);


                    adapter = new SqlDataAdapter(OrdersHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS");

                    // Establish a relationship between the two tables.
                    DataRelation relationOrdersClients = new DataRelation("OrdersClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ORDERS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationOrdersClients);

                    adapter = new SqlDataAdapter(DepartmentsHelper.selectAll(), con);
                    adapter.Fill(data, "DEPARTMENTS");

                    adapter = new SqlDataAdapter(EmployeesHelper.selectAll(), con);
                    adapter.Fill(data, "EMPLOYEES");

                    // Establish a relationship between the two tables.
                    DataRelation relationEmployeesDepatrments = new DataRelation("EmployeesDepatrments",
                        data.Tables["DEPARTMENTS"].Columns["DEPARTMENTS_ID"],
                        data.Tables["EMPLOYEES"].Columns["DEPARTMENTS_ID"]);
                    data.Relations.Add(relationEmployeesDepatrments);

                    adapter = new SqlDataAdapter(OrdersHelper.selectAll(), con);
                    adapter.Fill(data, "PRODUCTS");

                    adapter = new SqlDataAdapter(OrdersPositionsHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS_POSITIONS");

                    // Establish many-to-many relationship between tables.
                    DataRelation relationOrdersToPositions = new DataRelation("Products_OrdersToPositions",
                        data.Tables["PRODUCTS"].Columns["PRODUCTS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["PRODUCTS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);

                    relationOrdersToPositions = new DataRelation("Orders_OrdersToPositions",
                        data.Tables["ORDERS"].Columns["ORDERS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["ORDERS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        public static DataView getEmployees()
        {
            return data.Tables["EMPLOYEES"].AsDataView();
        }

        public static void updateEmployees()
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["MSSQL_home"].ConnectionString;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    // Create data adapter
                    SqlDataAdapter da = new SqlDataAdapter();

                    // create command
                    SqlCommand updateCmd = new SqlCommand(EmployeesHelper.updateEmployees(), conn);

                    //
                    // map parameters
                    //
                    SqlParameter parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "EMPLOYEES_NAME");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = updateCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "PHONE");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = updateCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "MAIL");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = updateCmd.Parameters.Add("@Salary", SqlDbType.Int, 10, "SALARY");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = updateCmd.Parameters.Add("@DeptID", SqlDbType.Int, 10, "DEPARTMENTS_ID");
                    parm.SourceVersion = DataRowVersion.Current;

                    // EmployeeID
                    parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 10, "EMPLOYEES_ID");
                    parm.SourceVersion = DataRowVersion.Current;

                    // Update database
                    da.UpdateCommand = updateCmd;
                    da.Update(data, "EMPLOYEES");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }

        }

        public static void insertEmployee()
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["MSSQL_home"].ConnectionString;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    // Create data adapter
                    SqlDataAdapter da = new SqlDataAdapter();

                    // create command
                    SqlCommand inserCmd = new SqlCommand(EmployeesHelper.insertEmployee(), conn);

                    //
                    // map parameters
                    //
                    SqlParameter parm = inserCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "EMPLOYEES_NAME");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = inserCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "PHONE");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = inserCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "MAIL");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = inserCmd.Parameters.Add("@Salary", SqlDbType.Int, 10, "SALARY");
                    parm.SourceVersion = DataRowVersion.Current;
                    parm = inserCmd.Parameters.Add("@DeptID", SqlDbType.Int, 10, "DEPARTMENTS_ID");
                    parm.SourceVersion = DataRowVersion.Current;

                    // EmployeeID
                    //parm = inserCmd.Parameters.Add("@ID", SqlDbType.Int, 10, "EMPLOYEES_ID");
                    //parm.SourceVersion = DataRowVersion.Current;

                    // Update database
                    da.InsertCommand = inserCmd;
                    da.Update(data, "EMPLOYEES");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static DataView getAccounts()
        {
            return data.Tables["ACCOUNTS"].AsDataView();
        }

        public static DataView getBanks()
        {
            return data.Tables["BANKS"].AsDataView();
        }

        internal static DataView getProducts()
        {
            return data.Tables["PRODUCTS"].AsDataView();
        }

        internal static DataView getOrders()
        {
            return data.Tables["ORDERS"].AsDataView();
        }

        internal static DataView getClients()
        {
            return data.Tables["CLIENTS"].AsDataView();
        }

        internal static DataView getDepartments()
        {
            return data.Tables["DEPARTMENTS"].AsDataView();
        }
    }
}
