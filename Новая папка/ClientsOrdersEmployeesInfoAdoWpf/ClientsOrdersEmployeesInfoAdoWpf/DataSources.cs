﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class EmployeesHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[EMPLOYEES]";
        }

        public static string updateEmployees()
        {
            return @"
                UPDATE [dbo].[EMPLOYEES]   
                SET
                   EMPLOYEES_NAME = @Name,
                   PHONE = @Phone,
                   MAIL = @Mail,
                   SALARY = @Salary,                
                   DEPARTMENTS_ID = @DeptID,
                WHERE
                    EMPLOYEES_ID = @ID
             ";
        }

        public static string insertEmployee()
        {
            return @"
                INSERT INTO [dbo].[EMPLOYEES]   
                (
                   EMPLOYEES_NAME,
                   PHONE,
                   MAIL,
                   SALARY,                
                   DEPARTMENTS_ID
                )
                VALUES
                (
                    @Name,
                    @Phone,
                    @Mail,
                    @Salary,
                    @DeptID
                )";
        }
    }

    public class DepartmentsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[DEPARTMENTS]";
        }
    }

    public class ClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[CLIENTS]";
        }
    }

    public class OrdersHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ORDERS]";
        }
    }

    public class ProductsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[PRODUCTS]";
        }
    }

    public class BanksHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[BANKS]";
        }
    }

    public class AccountsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ACCOUNTS]";
        }
    }

    public class OrdersPositionsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ORDERS_POSITIONS]";
        }
    }

    public class AccountsToClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ACCOUNTS_TO_CLIENTS]";
        }
    }
}
