﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            dg_Employees.ItemsSource = DBAccess.getEmployees();
            dg_Departments.ItemsSource = DBAccess.getDepartments();
            dg_Clients.ItemsSource = DBAccess.getClients();
            dg_Orders.ItemsSource = DBAccess.getOrders();
            dg_Products.ItemsSource = DBAccess.getProducts();
            dg_Banks.ItemsSource = DBAccess.getBanks();
            dg_Accounts.ItemsSource = DBAccess.getAccounts();
        }

        private void btn_viewEmployee_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Employees.SelectedIndex;
            if (index >= 0)
            {
                int ID = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[0];
                string name = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[1].ToString(); //empty field in view...
                string phone = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[2].ToString();
                string mail = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[3].ToString();
                int salary = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[4];
                int deptID = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[5];

                ViewEmployee viewEmployee = new ViewEmployee(ID, name, phone, mail, salary, deptID);
                viewEmployee.ShowDialog();
            }
        }

        private void btn_editEmployee_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Employees.SelectedIndex;
            if (index >= 0)
            {
                int ID = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[0];
                string name = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[1].ToString(); //empty field in view...
                string phone = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[2].ToString();
                string mail = DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[3].ToString();
                int salary = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[4];
                int deptID = (int)DBAccess.data.Tables["EMPLOYEES"].Rows[index].ItemArray[5];

                AddOrEditEmployee editEmployee = new AddOrEditEmployee(ID, name, phone, mail, salary, deptID);
                editEmployee.ShowDialog();
            }
        }

        private void btn_addEmployee_Click(object sender, RoutedEventArgs e)
        {
            AddOrEditEmployee editEmployee = new AddOrEditEmployee();
            editEmployee.ShowDialog();
        }

        private void dg_Departments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_EmplDepts.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["EmployeesDepatrments"]);
            }
            catch (Exception)
            { }
        }

        private void dg_Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_ClientsOrders.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["OrdersClients"]);
            }
            catch (Exception)
            { }
        }

        private void dg_Banks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_BanksAccounts.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["BanksAccounts"]);
            }
            catch (Exception)
            { }
        }

       
    }
}
