﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for ViewEmployee.xaml
    /// </summary>
    public partial class ViewEmployee : Window
    {
        private ViewEmployee()
        {
            InitializeComponent();
        }

        public ViewEmployee(int ID, string name, string phone, string mail, int salary, int deptID)
        {
            InitializeComponent();
            txtID.Text = ID.ToString();
            txtName.Text = Name;
            txtPhone.Text = phone;
            txtMail.Text = mail;
            txtSalary.Text = salary.ToString();
            txtDeptID.Text = deptID.ToString();
        }
    }
}
